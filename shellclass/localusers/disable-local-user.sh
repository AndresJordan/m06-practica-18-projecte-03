#!/bin/bash

# Función para mostrar el man del script
man() {
        echo -e "Exit status 1: Debes introducir al menos un parametro."
        echo -e "Usage: ./$(basename $0) [-dra] USER [USERN]"
        echo -e "Disable a local Linux account."
        echo -e "\t-d Deletes accounts instead of disabling them."
        echo -e "\t-r Removes the home directory associated with the account(s)."
        echo -e "\t-a Creates an archive of the home directory associated with account(s)."
}

# Función para borrar el usuario
funcion_d() {
        echo -e "Proccesing user: $1"
        userdel $1
        echo -e "The account $1 was deleted"
}

# Función para borrar el directorio home
funcion_r() {
        rm -rf /home/$1
}

# Función para archivar el home a /archive
funcion_a(){
        echo -e "Creating /archive directory."
        mkdir -p /archive/
        echo -e "Archiving /home/$1 to /archive/$1.tgz"
        tar fczP $1.tgz /home/$1/
        mv $1.tgz /archive/
}

# Nos aseguramos que ejecute como root el script
if [[ "${UID}" -eq 0 ]]; then
        # Si no pos indica menos de un argumento le mostramos el man
        if [ "$#" -lt 1 ]; then
                man
        # Si empieza con un caracter de la a-z o A-Z es que hay solo un parametro
        elif [[ $1 =~ ^[a-zA-Z] ]]; then
                # Obtenemos el UID del usuario
                UID_USUARIO=$(cat /etc/passwd | grep "^$1:" | cut -f 3 -d ':')
                # Comprobamos que no sea más pequeño que 1001
                if [ "$UID_USUARIO" -lt 1001 ]; then
                        echo -e "Processing user: $1"
                        echo -e "Refusing to remove the account with UID $UID_USUARIO"
                else
                        # Bloqueamos la cuenta de usuario
                        echo -e "Processing user: $1"
                        chage -E 1970-01-01 $1
                        echo -e "The account $1 was disabled"
                fi
        else
                # Almacenmos las opciones escogidas y los argumentos por cada opción
                opciona=false
                opciona_arg=""
                opcionr=false
                opcionr_arg=""
                opciond=false
                opciond_arg=""
                while getopts ":a:r:d:" OPTION
                do
                        case $OPTION in
                                a)
                                        opciona=true
                                        opciona_arg=$OPTARG
                                        ;;
                                r)
                                        opcionr=true
                                        opcionr_arg=$OPTARG
                                        ;;
                                d)
                                        opciond=true
                                        opciond_arg=$OPTARG
                                        ;;
                                \?)
                                        man
                                        ;;
                                :)
                                        man
                                        ;;
                        esac
                done
                # Si se ha escogido la función la llamamos pasandole el argumento es importante mantener este orden de primero archivar, borrar home y borrar usuario.
                if $opciona; then
                        funcion_a $opciona_arg
                fi
                if $opcionr; then
                        funcion_r $opcionr_arg
                fi
                if $opciond; then
                        funcion_d $opciond_arg
                fi
        fi
else
        echo 'Exit status 1: No eres usuario root.'
fi
