# PRACTICA 18 - PROJECTE 03 #
Deshabilita un usuario existente
Hace la copia de seguridad de la carpeta de un usuario existente.
Elimina la carpeta del home de un usuario existente.
Suprime del sistema un usuario existente.

## Requisitos previos

* Instalar Linux
* Instalar Vagrant

## Pasos para la instalación

Para empezar la instalación hay que descargar los archivos necesarios del respositorio de Bitbucket.

```
git clone https://AndresJordan@bitbucket.org/AndresJordan/m06-practica-18-projecte-03.git
```

Este respositorio contiene el fichero script en shellclass/localusers/:

```
disable-local-user.sh
```

### disable-local-user.sh

Script que contiene el código. Esta situado en /home/vagrant/.

## Pasos para la ejecución

Situados en el mismo directorio ejecutamos el siguiente comando.

```
vagrant up
```

## VirtualBox

```
User: vagrant
Password: vagrant
```